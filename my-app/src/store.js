import Vue from 'vue'
import Vuex from 'vuex'
import client from 'api-client'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    posts: [
      {
        url: 'abc.com',
        response: {
          name: 'name 1',
          price: '1usd'
        }
      },
      {
        url: 'xyz.com',
        response: {
          name: 'name 2',
          price: '2usd'
        }
      },
      {
        url: 'azx.com',
        response: {
          name: 'name 3',
          price: '3usd'
        }
      }
    ]
  },
  mutations: {
    setPosts (state, posts) {
      state.posts = posts
    }
  },

  actions: {
    fetchPosts ({ commit }) {
      return client
        .fetchPosts()
        .then(posts => commit('setPosts', posts))
    }
  }
})
