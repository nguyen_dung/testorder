import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import CreateAnOrder from '@/components/order/CreateAnOrder'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/create-an-order',
      name: 'CreateAnOrder',
      component: CreateAnOrder
    }
  ]
})
